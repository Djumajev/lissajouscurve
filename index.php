<!DOCTYPE>
<html>
    <head>

        <style>
            body{
                height: 100vh;
                width: 100vw;
                background-color: darkgray;
            }

            canvas{
                width: 800px;
                height: 600px;
                margin: auto;
                background-color: #000;
            }

            ui{
                width: 35%;
                display: block;
                float: left;
                text-align: center;
            }

            screen{
                width: 800px;
                display: block;
                float: left;
            }

        </style>
    </head>

    <body>

        <screen>
            <canvas id=Canvas width=800 height=600></canvas>
        </screen>

        <script>
            const colors = {
                RED: "#FF0000",
                GREEN: "#00FF00",
                BLUE: "#0000FF",
                WHITE: "#FFFFFF",
                YELLOW: "#FFFF00"
            }

            var c = document.getElementById("Canvas");
            var ctx = c.getContext("2d");
            var speed = 0.0001;

            var wheelSpeed1 = 1, wheelSpeed2 = 1;
            var wheelSize1 = 50, wheelSize2 = 50;

            var figure1Type = 1, figure2Type = 1;

            class circle{
                constructor(x, y, r, color){
                    this.x = x;
                    this.y = y;
                    this.r = r;
                    this.color = color;
                }

                draw(){
                    ctx.strokeStyle=colors[this.color];
                    ctx.beginPath();
                    ctx.arc(this.x,this.y,this.r,0,2*Math.PI);
                    ctx.stroke();
                }

                drawFigureA(){
                    for(let i = 0; i <= 360; i+=10){
                        let X = this.x + this.r * Math.pow ( Math.cos( (i*180/Math.PI) ), 3 );
                        let Y = this.y + this.r * Math.pow (  Math.sin( (i*180/Math.PI) ), 3);
                        new dot(X, Y, "WHITE").draw();
                    }
                }
            }
                        
            class line{
                constructor(x1, y1, x2, y2, color){
                    this.x1 = x1;
                    this.y1 = y1;
                    this.x2 = x2;
                    this.y2 = y2;
                    this.color = color;
                }

                draw(){
                    ctx.strokeStyle=colors[this.color];
                    ctx.beginPath();
                    ctx.moveTo(this.x1,this.y1);
                    ctx.lineTo(this.x2,this.y2);
                    ctx.stroke();
                }
            }
            
            class dot{
                constructor(startX, startY, color){
                    this.x = startX;
                    this.y = startY;
                    this.startX = startX;
                    this.startY = startY;
                    this.counter = 0;
                    this.color = color;
                }

                draw(){
                    ctx.fillStyle=colors[this.color];
                    ctx.beginPath();
                    ctx.fillRect(this.x,this.y,1,1);
                    ctx.arc(this.x,this.y,3,0,2*Math.PI);

                    ctx.fill();
                }

                drawRectOnly(size){
                    ctx.fillStyle=colors[this.color];
                    ctx.beginPath();
                    ctx.fillRect(this.x,this.y,size,size);

                    ctx.fill();
                }

                calcCircleMovement(r, speedk = 1){
                    this.x = this.startX + r * Math.cos( (this.counter*180/Math.PI) * speed * speedk);
                    this.y = this.startY + r * Math.sin( (this.counter*180/Math.PI) * speed * speedk);
                    this.counter++;
                }

                calcFigureAMovement(r, speedk = 1){
                    this.x = this.startX + r * Math.pow ( Math.cos( (this.counter*180/Math.PI) * speed * speedk), 3 );
                    this.y = this.startY + r * Math.pow (  Math.sin( (this.counter*180/Math.PI) * speed * speedk), 3);
                    this.counter++;
                }
            }


            var circle1 = new circle(300, 300, wheelSize1, "WHITE");
            var circle2 = new circle(300 + circle1.r*2, 300 - circle1.r*2, wheelSize2, "WHITE");
            var dot1 = new dot(circle1.x , circle1.y, "GREEN");
            var dot2 = new dot(circle2.x, circle2.y, "GREEN");
            var path = new Array();

            var line1, line2;

            function draw(){
                ctx.clearRect(0, 0, c.width, c.height);
                
                if(+figure1Type == 1)
                    circle1.draw();
                else circle1.drawFigureA();
                
                if(figure2Type == 1)
                    circle2.draw();
                else circle2.drawFigureA();
                
                if(figure1Type == 1)
                    dot1.calcCircleMovement(circle1.r, wheelSpeed1);
                else dot1.calcFigureAMovement(circle1.r, wheelSpeed1);

                if(figure2Type == 1)
                    dot2.calcCircleMovement(circle2.r, wheelSpeed2);
                else dot2.calcFigureAMovement(circle2.r, wheelSpeed2);

                dot1.draw();
                dot2.draw();

                line1 = new line(dot1.x, dot1.y, 800, dot1.y, "BLUE");
                line2 = new line(dot2.x, dot2.y, dot2.x, 600, "YELLOW");
                line1.draw();
                line2.draw();

                for(let i = 0; i < path.length; i++)
                    new dot(path[i].x, path[i].y, "RED").drawRectOnly(1);

                let x1 = Math.floor(dot1.x), y1 = Math.floor(dot1.y), 
                    x2 = Math.floor(dot2.x), y2 = Math.floor(dot2.y);
                let isPoint = false;

                while(!isPoint){
                    if(x1 == x2){
                        new circle(x1, y1, 10, "GREEN").draw();
                        path.push({
                            x: x1,
                            y: y1
                        });

                        isPoint = true;
                    } 
                    
                    x1++;
                    y2++;

                }

                window.requestAnimationFrame(draw);
            }


            function update(){
                wheelSize1 = document.getElementById("size1").value;
                wheelSize2 = document.getElementById("size2").value;
                wheelSpeed1 = document.getElementById("speed1").value;
                wheelSpeed2 = document.getElementById("speed2").value;

                var radios = document.getElementsByName('type1');
                if(radios[0].checked)
                    figure1Type = 1;
                else figure1Type = 2;

                var radios = document.getElementsByName('type2');
                if(radios[0].checked)
                    figure2Type = 1;
                else figure2Type = 2;

                path = new Array();

                circle1 = new circle(300, 300, wheelSize1, "WHITE");
                circle2 = new circle(300 + circle1.r*2, 300 - circle1.r*2, wheelSize2, "WHITE");
                dot1 = new dot(circle1.x , circle1.y, "GREEN");
                dot2 = new dot(circle2.x, circle2.y, "GREEN");
                path = new Array();
            }


            window.onload = function() {
                draw("RED");
            };

        </script>

        <ui>
            Type of Figure 1: <br> <input name=type1 type="radio"> Circle <br>
                                    <input name=type1 type="radio"> Figure2 <br>
            Type of Figure 2: <br> <input name=type2 type="radio"> Circle <br>
                                    <input name=type2 type="radio"> Figure2 <br>


            <form onsubmit="return false" oninput="level.value = flevel.valueAsNumber">
                First wheel size<input name="flevel" id=size1 type="range" min="1" max="100" step="1" value="50">
                <output for="size1" name="level">50</output>
            </form>

            <form onsubmit="return false" oninput="level.value = flevel.valueAsNumber">
                Second wheel size<input name="flevel" id=size2 type="range" min="1" max="100" step="1" value="50">
                <output for="size2" name="level">50</output>
            </form>

            <form onsubmit="return false" oninput="level.value = flevel.valueAsNumber">
                First wheel speed<input name="flevel" id=speed1 type="range" min="1" max="10" step="1" value="1">
                <output for="speed1" name="level">1</output>
            </form>

            <form onsubmit="return false" oninput="level.value = flevel.valueAsNumber">
                Second wheel speed<input name="flevel" id=speed2 type="range" min="1" max="10" step="1" value="1"> 
                <output for="speed2" name="level">1</output>
            </form>

            <button onmousedown="update()">Start</button>
        </ui>


    </body>


</html>

